mkfile_path := $(abspath $(lastword $(MAKEFILE_LIST)))
root_dir := $(patsubst %/,%,$(dir $(mkfile_path)))

# Relativo al target, no al root
include $(root_dir)/config.mk

/etc/systemd/system/multi-user.target.wants/%.service:
	systemctl enable $(notdir $@)

/etc/systemd/system/sockets.target.wants/%.socket:
	systemctl enable $(notdir $@)

# Plantillas!
# 
# Las plantillas usan ${variable} y terminan en .in.
#
# Para generarlas, hay que hacer que las 
%: %.in
	envsubst <$< >$@

refresh-templates:
	touch *.in

must_be := test `cat` -eq

# Comprueba que el archivo exista
file_exists? := test -f

# Comprueba que el directorio exista
dir_exists? := test -d

# Obtiene el dueño de un archivo
stat_user  := stat --printf="%U"
# Obtiene el grupo de un archivo
stat_group := stat --printf="%G"
# Obtiene los permisos de un archivo
stat_perm  := stat --printf="%a"

# Verificar que el grupo exista
is_group := getent group
# Verificar que el usuario exista
is_user  := getent passwd

# Obtiene el directorio de un archivo sin la / al final
dir_no_slash = $(patsubst %/,%,$(dir $1))

# Comprueba que una variable requerida no este vacia
# $(call argument_missing,variable)
argument_missing = @if test -z "$($(1))" ; then echo "Parámetro faltante: $(1)" ; false; fi

# Genera una contraseña aleatoria de rango determinado
# $(shell $(generate_password_of)32)
generate_password_of = dd if=/dev/urandom bs=256 count=1 2>/dev/null | base64 | tr -d "\n" | sed "s,/,,g" | cut -c-

# Corre el comando como un usuario sin privilegios especiales
unprivileged_user := $(firstword $(USERS))
as_unprivileged := sudo -u $(unprivileged_user)

# Corre comandos dentro de la chroot
run_mnt := arch-chroot $(mnt)

# Siempre correr all
.DEFAULT_GOAL := all

help: ## Ayuda
	@grep -hE '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) \
		| sed -r 's/:.*## /\t/' \
		| while read -r target message; do \
		  printf "%s\t%s\n" "$$target" "$$message" ;\
	done

# Un target que obliga al target que lo llama a ejecutarse cada vez
always:
.PHONY: always
