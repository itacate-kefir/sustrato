# Sustrato

Un sustrato para que crezcan nuestros hongos :)

## Qué es

`sustrato` es un set de `Makefiles` para crear un anfitrión KVM seguro.
Adapta algunas ideas de
[`kvm-manager`](https://0xacab.org/dkg/kvm-manager) a `systemd`.

## Cómo se le hace

### Configuración

Edita `config.mk` para adaptarlo a tus necesidades.

### RAID1+0

Los discos se configuran en RAID1+0 usando Linux MD, de forma de tener
redundancia y performance.

<https://en.wikipedia.org/wiki/Non-standard_RAID_levels#LINUX-MD-RAID-10>

```bash
  make -C raid
```

### FDE con LUKS

El cifrado de disco completo [Full Disk Encryption] crea una capa de
cifrado para todos los datos que se almacenen en el disco, exceptuando
`/boot`.

```bash
  make -C fde
```

### LVM

[LVM](https://es.wikipedia.org/wiki/Logical_Volume_Manager) se encarga
de mantener los volúmenes (particiones) donde se alojan el sistema
anfitrión y los invitados.
