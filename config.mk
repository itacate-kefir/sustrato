# # Configuración

# El nombre de este servidor!
HOSTNAME ?= $(shell hostname)

# Enviar mails administrativos a esta dirección
mail ?= $(HOSTNAME)@kefir.red
# Enviar todos los mails a este servidor
relay ?=

# La interfaz de red con salida a Internet
#
# XXX si estamos usando OpenVPN van a salir dos interfaces!
eth ?= $(shell ip route show match 0/0 | cut -d ' ' -f 5)
# La interfaz virtual que uno a todos los guests
br  ?= kvm0

# Configuración de red para initramfs
#
# XXX mkinitcpio-netconf no soporta DHCP
ip   ?=
gw   ?=
dns  ?=
net  ?=
# Todavía no tenemos udev acá, así que las interfaces tienen los nombres
# que les da el kernel: ethX wlanX
dev  ?= eth0
cidr ?=

# La configuración de red es estática o dinámica?
#
# Valores posibles: static dhcp
net_type ?= static

# Red del host KVM
#
# Si la red es "nat", el host KVM actúa como un router.  La red es un
# rango privado y los guests no son alcanzables por Internet.  Usar esta
# opción cuando solo tengamos una dirección IP para el host y todos sus
# guests.
#
# Si la red es "bridge" y disponemos de un rango de direcciones IP
# públicas, el host posee la primera IP del rango y asigna las demás a
# los guests.  En lugar de actuar como un router, el host recibe todos
# los paquetes y cada guest se encarga de recibir los suyos.
#
# XXX crear un modo hibrido donde algunos guests son públicos y otros
# privados
#
# Las IPs, públicas o privadas, se asignan por DHCP, por lo que los
# guests no requieren mayor configuración.
#
# Valores posibles: bridge nat
kvm_network_type ?= bridge

# La IPv4 del bridge y su netmask.  Especificar si la red es NAT, si es
# bridge, se autoconfigura a partir de los parámetros ip y cidr más
# arriba.
kvm_network    ?= 192.168.100
bridge_address ?= $(kvm_network).1
bridge_cidr    ?= 24

# Opciones para DHCP
dhcp_ttl    ?= 1w
dhcp_range  ?= $(kvm_network).2,$(kvm_network).254,$(dhcp_ttl)
dhcp_domain ?= $(HOSTNAME).local

kvm_etc_dir := /etc/kvm
kvm_run_dir := /run/kvm
# Este archivo contiene los leases fijos de DHCP en el formato
# mac,ip,hostname,lease
kvm_dhcp_leases := $(kvm_etc_dir)/dhcp.conf

# Asignar is_bridge o is_nat dependiendo del tipo de red
ifeq (bridge,$(kvm_network_type))
is_bridge := true
else
is_nat    := true
endif

# ## Discos
#
# La ubicación base de los dispositivos RAID creados por mdadm
md_base := /dev/md
# Todos los discos que van a estar dentro del RAID
md_disks := /dev/sda /dev/sdb
# El RAID para /boot
md_boot := $(md_base)/boot
# El RAID para /
md_root := $(md_base)/root
# La swap
md_swap ?= $(patsubst %,%3,$(md_disks))
# Tamaño de la swap
swap_size ?= 4G

# La raíz cifrada
root := /dev/mapper/root
# El volumen LVM
vg   := /dev/mapper/$(HOSTNAME)
lv_root_name := host-root
lv_root := /dev/$(HOSTNAME)/$(lv_root_name)
# Cambiar a `-l +100%FREE` para ocupar todo el espacio
lv_root_size := -L 20G

# El punto de montaje para la instalación
mnt  ?= /mnt/$(HOSTNAME)

# ## LUKS
# Algoritmo de cifrado
cipher := aes-xts-plain64
# Hash
hash := sha512
# Tamaño de llave
key_size := 512
# Iteraciones de hasheo
iter := 5000
# Flags pasadas a cryptsetup
luks_options := -c $(cipher) -s $(key_size) -h $(hash) -i $(iter)

# Todas las variables se exportan al resto de makefiles
export
