include ../macros.mk

# Número de orden de las particiones
boot_order := 1
swap_order := 2
root_order := 3

# Esta magia crea un target /dev/sdb-zap que no existe y le elimina el
# -zap para poder llamar al original con $*.  Hay que tener cuidado
#  porque al ser targets que no existen, se ejecutan cada vez.
zap_md := $(patsubst %,%-zap,$(md_disks))
$(zap_md): %-zap:
	sgdisk --zap-all $*

# Crea las particiones de tipo GPT para cada disco, incluyendo booteo.
# Esto hace que cualquier disco sea booteable.
#
# * 128M para /boot
# * LUKS lo que sobre
gpt_disks := $(patsubst %,%-gpt,$(md_disks))
$(gpt_disks): %-gpt: fdisk
	cat $< | fdisk $*
	partprobe $*
	rm -f $<

# /dev/sdX3: swap
#
# Cada disco tiene una partición swap cifrada
#
# https://wiki.archlinux.org/index.php/Dm-crypt/Swap_encryption
swap_parts := $(patsubst %,%$(swap_order)-swap,$(md_disks))
$(swap_parts): %-swap:
	mkfs.ext2 -L cryptswap $* 1M
	
# Crear un backup de la tabla de particiones
backup_parts := $(patsubst %,%-backup,$(md_disks))
$(backup_parts): %-backup:
	sgdisk --backup=../backup/$(notdir $*).gpt $*

zap: $(zap_md)           ## Elimina los datos GPT y MBR de todos los discos
gpt: $(gpt_disks)        ## Crea la GPT en los discos
swap: $(swap_parts)      ## Crea la swap cifrada
backup: $(backup_parts)  ## Crea un backup!
all: zap gpt swap backup      ## Todo
